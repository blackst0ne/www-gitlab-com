---
layout: markdown_page
title: "IR.2.01 - External Communication of Incidents Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# IR.2.01 - External Communication of Incidents

## Control Statement

GitLab defines external communication requirements for incidents, including:

* Information about external party dependencies.
* Criteria for notification to external parties as required by GitLab policy in the event of a security breach.
* Contact information for authorities (e.g., law enforcement, regulatory bodies, etc.).
* Provisions for updating and communicating external communication requirement changes.

## Context

This control demonstrates that we have documented how we will communicate externally in the event of an incident.  This helps the company by making sure we will contact the necessary external parties.

## Scope

TBD

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLabbers, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IR.2.01_external_communication_of_incidents.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IR.2.01_external_communication_of_incidents.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IR.2.01_external_communication_of_incidents.md).

## Framework Mapping

* ISO
  * A.6.1.3
* PCI
  * 12.10.1
