---
layout: markdown_page
title: "CEO shadow program"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

The CEO shadow program at GitLab is not a job title but a temporary assignment to shadow the CEO.
The shadow(s) will be present at all meetings of the CEO. GitLab is [all remote](/company/culture/all-remote/) but sometimes the CEO has in-person meetings. Therefore, you will live in San Francisco during the entire [rotation](#rotation) and travel with the CEO. 

## Goal

The goal of the shadow program is to give current and future [directors and senior leaders](/company/team/structure/) at GitLab a overview of all aspects of the [company](/company/).
This should lead to leadership who are able to do [global optimizations](/handbook/values/#global-optimization).

## Attendence

You will attend all meetings of the CEO, this includes:

1. 1-1s with reports
1. interviews with applicants
1. conversations with board members

The CEO's executive assistant should ask external people if 
they are comfortable with the shadow joining prior to the scheduled meeting and will share
a link to the CEO Shadow page to provide context. 

These meetings can have different formats:

1. video calls
1. in-person meetings.
1. dinners that are business related
1. customer visits
1. conferences

You will not attend when:

1. Someone wants to discuss a complaint and wants to stay anonymous.
1. If any participant in the meeting is uncomfortable.
1. If the CEO wants more privacy.

This is probably the most open program in the world.
It depends on the participants respecting confidentiality, during the program, after the program, and after they leave GitLab.

## Rotation

We want many people to be able to benefit from this program, therefore we rotate often.
It is also important that an incoming person is trained so that the management overhead can be light.
Therefore we decided to make a rotation two weeks:

1. See one, you are trained by the outgoing person.
1. Teach one, you train the incoming person.

In general there should be no planned holidays of the shadown during the period unless the CEO is also on holiday.

### Planned rotation

| Start date | End date | Name | Title |
| ------ | ------ | ------ | ------ |
| 2019-03-18 | 2019-04-05 | Erica Lindberg | Manager, Content Marketing |
| 2019-04-01 | 2019-04-05 | Michael Fahey | Security Manager, Red Team |
| 2019-04-15 | 2019-05-03 | Mayank Tahil | Alliances Manager |
| 2019-04-29 | 2019-05-17 | Joe Zelazny | Learning and Development Partner, PeopleOps |
| 2019-05-13 | 2019-05-31 | No shadow | [Contribute](/company/culture/contribute/) | 
| 2019-05-27 | 2019-06-14 | TBD | TBD |
| 2019-06-10 | 2019-06-21 | TBD | TBD |
| 2019-06-17 | 2019-06-28 | TBD | TBD |
| 2019-06-24 | 2019-07-12 | TBD | TBD |
| 2019-06-30 | 2019-07-07 | No shadow | Vacation |
| 2019-07-08 | 2019-08-02 | TBD | TBD |
| 2019-07-14 | 2019-07-29 | No shadow | Vacation |
| 2019-07-30 | 2019-08-09 | TBD | TBD |
| 2019-08-05 | 2019-08-16 | TBD | TBD |
| 2019-08-12 | 2019-08-23 | TBD | TBD |
| 2019-08-12 | 2019-08-23 | TBD | TBD |
| 2019-08-19 | 2019-09-06 | TBD | TBD |
| 2019-08-26 | 2019-09-03 | No shadow | Vacation |
| 2019-09-04 | 2019-09-13 | TBD | TBD |

## What to expect

1.  This isn't a performance evaluation, so get comfortable
1.  You do not need to dress formally, so business casual clothes are appropriate. For example, Sid wears a button up with jeans.
1.  Review Sid's calendar to get an idea of what your upcoming weeks will be like
1.  Review and update the ongoing CEO agenda. This agenda contains TODOs, documentation items, training, and feedback information items.
1.  Be ready to observe and ask questions
1.  Give and receive feedback from Sid

## Tasks

Since a rotation is over a short period there are no long running tasks that you can assume.

The tasks consist of short term tasks, for example:

1. Make [handbook](/handbook/) updates
1. Draft email responses
1. Solve urgent issues, for example a complaint from a customer or coordinating the response to a technical issue.
1. Prepare meetings.
1. Take notes during meetings.
1. Follow up on meetings (improve the notes, followup appointment, thank you cards, swag).
1. Ensure that internal teams are updated with action items and outcomes
1. Compile a report on a subject.
1. Write a blog post based on a conversation.
1. Editing a recorded video.
1. Promoting the work you created.
1. Writing a blog post about something you learned or your experience.
1. Camerawork for a video.
1. Control slides and muting during the board meeting.
1. Set up for an in-person meeting.
1. Receive guests.
1. Add to the documentation of GitLab.
1. Do and publish a [CEO interview](https://gitlab.com/gitlab-com/www-gitlab-com/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=CEO%20interview).
1. Add training for this program to the handbook.
1. Training the incoming shadow program person.
1. Speak up when the [CEO displays flawed behaviour](https://about.gitlab.com/handbook/ceo/#flaws).

## Naming

For now this role is called a [CEO shadow](https://feld.com/archives/2015/03/ceo-shadowing.html) to make it clear to external people why a shadow is in a meeting.

Other names considered are:

1. Technical assistant. Seems confusing with [executive assistant](/job-families/people-ops/executive-assistant/). ["In 2003, Mr. Bezos picked Mr. Jassy to be his technical assistant, a role that entailed shadowing the Amazon CEO in all of his weekly meetings and acting as a kind of chief of staff. "](https://www.theinformation.com/articles/amazons-cloud-king-inside-the-world-of-andy-jassy)
1. Chief of Staff. This commonly is the ["coordinator of the supporting staff"](https://en.wikipedia.org/wiki/Chief_of_staff) which is not the case for this role since people rotate out of it frequently. The executive assistants reports to peopleops.
1. [Global Leadership Shadow Program](https://www2.deloitte.com/ng/en/pages/careers/articles/leadership-shadow-program.html) is too long if only the CEO is shadowed.

## Documentation

A ongoing shadow program with a fast rotation is much more time consuming for the CEO then a temporary program or a rotation of a year or longer.
Therefore most organizations either have a shadow for two days or have someone for a year or more.
We want to give many people the opportunity to be a shadow so we rotate quickly.
To make this happen without having to invest a lot of time to train people coming in we need great documentation.
Therefore a quick turnaround on documentation is of paramount importance.
And the documentation will have a level of detail that isn't needed in other parts of the organization.

## Eligible

If you are a current or future [directors and senior leaders](/company/team/structure/) at GitLab you can apply.
For new team members this might be the first thing they do after completing our [onboarding](/handbook/general-onboarding/).
To apply for the program please ask your manager.

## Training checklist 

Outgoing shadows are responsible for training incoming shadows. Here's a list of things to make sure you cover:

1. Boardroom location & WiFi.
1. Set a meeting time and place for the incoming Shadow's first day. The Outgoing Shadow will give the Incoming Shadow access to the boardroom. 
1. At the start of the week, review your calendar and Sid's. If a meeting wasn't added to your calendar and looks like it should be, reach out to Cheri to confirm if you should be invited or not. There will be some meetings and events the Shadow does not attend. 
1. Discuss coordinating schedules and key access to avoid ringing Sid.
1. How to set up the board room for in-person meetings (boardroom TV, answering the phone, greeting the guest).
1. How to update the boardroom TV screens.
1. When to show up to the boardroom, getting to events, dinners, etc. 
1. How to set up a livestream. Make sure you are added as a co-host for any planned livestreams ahead of time.  
1. Encure incoming Shadow has access to the CEO Shadow agenda and knows how to make changes to the handbook.

### When to show up to the boardroom 

You are welcome to work from the boardroom but its not required to be present in-person unless there is an in-person meeting, event, or dinner. It's up to you to manage your schedule and get to places on time. If you are traveling somewhere, meet Sid at the boardroom at the beginning of the alloted travel time listed on the calendar.

If there is a day during your program where all meetings are Zoom meetings, you can work from whereever you want, as your normally would. You can work from the boardroom if you prefer. If you decide to split your day between remote work and working from the boardroom, make sure you give yourself enough time to get to the boardroom and set up for the guest. It's OK to join calls while mobile.

### Boardroom access

When entering the building, the doorperson will ask who you are there to see. Don't say "GitLab" since there is no GitLab office. The doorperson will direct you to the correct lobby.

There is one set of keys so the shadows will need to coordinate access to the boardroom. The fob is for the elevator and the key is for the boardroom.

## Boardroom TV

We have monitors in the boardroom. One of them has our sales dashboard, you can configure this with:

1. Go to Clari
1. Go To Pulse tab
2. Open the left side bar
3. Click on the funnel icon. Select “CRO”
4. Click on the gear icon. Go to Forecasting. Select Net IACV.

Use one monitor for 'this quarter' and one for 'next quarter' (blue link near the top).

### Switching the mouse and keyboard control

The wireless mouse and keyboard are connected to the middle, bottom TV by default. To update the view on another TV, you have to connect the wireless keyboard and mouse to the desired screen.

1. Find the USB jacks and Logitech receiver underneath the bottom, right TV.
1. Connect the Logitech receiver to USB receiver for the desired screen. 

## Video updates

1. [Day 2 of Erica Lindberg](https://www.youtube.com/watch?v=xrWR0uU4nbQ)
