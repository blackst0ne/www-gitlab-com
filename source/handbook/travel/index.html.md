---
layout: markdown_page
title: "Travel"
---

----

## On this page
{:.no_toc}

- TOC
{:toc}

----



## Expenses While Traveling <a name="expenses-while-traveling"></a>

1. The company will cover all work-related travel expenses. This includes lodging and meals during the part of the trip that is work-related. Depending on the distance of your travel, it can also include one day before and one day after the work related business. For example, if you are attending a 3 day conference in a jetlag-inducing location, the company will cover your lodging and meals those 3 days as well as one day before and one day after.
1. The company can accommodate custom requests. It is OK to stay longer on your trip. However, the extra days will not be covered by the company.
1. Understand that the guidelines of [Spending Company Money](/handbook/spending-company-money), especially the part about spending it like it is your own money, still apply for all expenses.
1. Always bring a credit card with you when traveling for company business if you have one.
1. Hotels will generally expect you to have a physical credit card to present upon check-in. This credit card will be kept on file for the duration of your stay. Even if your lodging was pre-paid by the company or by using a company credit card, the Hotel may still require a card to be on file for "incidentals".
1. If you incur any work-travel related expenses (on your personal card or a GitLab company card), please make sure to save the original receipt.
1. When your trip is complete, please file an expense report via Expensify or include the receipts on your next monthly invoice.

## Self Stay Incentive Policy <a name="self-stay"></a>

If you decide to stay with friends or family instead of a hotel / Airbnb for a business trip, the company will pay you 50% of what would have been
the cost of a reasonable hotel. This is in keeping with our values of frugality and freedom, and intended to reward you for saving company money on
accommodation expenses. Contractors should take a screenshot of a hotel search from the same location in NexTravel and submit this along with your expense
report or invoice.  Employees will be paid through payroll by submitting the screenshot by email to People Operations for processing.

## Booking travel and lodging<a name="NexTravel"></a>

Before booking any flights please ensure that you have the proper [visas](/handbook/people-operations/visas/#arranging-a-visa-for-travel-) in place.

### Setting up your NexTravel account<a name="setup-nextravel"></a>

Below is some more information to help you get set up with your [NexTravel](https://www.nextravel.com) account.  

1. Follow the link in your onboarding issue to create your NexTravel account and set a secure password. (Contact People Ops if you need help finding the link).
1. Once you are in your dashboard, make sure to set up your info in your profile and the preferences tab (see link at top middle right corner of page).
1. Add your passport information to your profile (only passport information is accepted), this will populate your info directly when booking a flight.
1. Now let's start booking!

### Booking travel through NexTravel <a name="booking-travel"></a>

If you book through [NexTravel](https://www.nextravel.com), the costs will be added
to the GitLab invoice and **no credit card is needed**.

1. Please note that some budget airlines might not show, so make sure
to check those to be sure there is no better option out there. (SouthWest, JetBlue, etc.).
1. Login to your account. If you've not yet set up your account, do this [first](#setting-up-your-nextravel-account).
1. The dashboard gives you the option to start a booking directly. The options are
to book flights, hotels, or rental cars.
1. Input info for the flight you want to book. You can pick return as a combo or each leg separately
1. Select "continue to checkout" and fill out the reasons for booking.
1. Once confirming all details are correct you can select "complete booking" and the flight will go for approval to your manager

1. You can add a hotel or car by clicking the boxes between the flight details and "complete booking" and go through the same steps.

Note: Once a flight is booked, re-routing is at person’s own cost unless requested by the company or force majeure (in the latter case often the airline will cover the difference).

#### FAQ about travel
Please read through these FAQ **entirely**, before contacting travel@. **They won't respond to these FAQ sent over email to them**.

1. If you have trouble finding travel within a budget, contact **your manager** about this, _not_ travel@
1. If your flight hasn't been approved, contact **your manager**, they are responsible, _not_ travel@
1. If your flight does not have a seat selection or checked bags included, you need to arrange that with **the airline**. Usually done upon check-in onlune, 24H before your flight.
1. If part of your travel has been changed after you've booked, contact **NexTravel Support** about this, they can help.

#### Booking shows Out of Policy

- Sometimes there is no way around booking a flight or hotel that is out of policy.
- This means that the option selected is more expensive than the cheapest option + our buffer.
- When you try to book this, an email will be sent to your manager for approval.
- Please provide extensive reasoning to make approval easier and faster.
- After 24 hours without any manual action (approved/denied) the booking will automatically be accepted.

## Travel Insurance

GitLab offers travel insurance for business travel. Full details of GitLab's Business Travel Accident Policy can be found in the [benefits section](../benefits/#general-benefits).

## Booking accommodations through Airbnb <a name="airbnb"></a>

- If you need an Airbnb for your stay on a business related trip, you can either book it yourself, or send a request to People Ops.
- For People Ops, please include a link to which Airbnb you would like to stay in, the corresponding dates, and the purpose of your travel.
- If you are booking the trip yourself, you may be reimbursed for the portion of your stay that was business related by submitting in Expensify or on your monthly invoice.

### Tips on Working Remotely While Abroad

Working remotely while being abroad can be quite different. For example it can be harder to arrange good working internet and you may need extra tools to have a similar level of comfort to be able to work effectively.

#### Flights

Planning flights far in advance can help you get cheaper fares when booking. For example you can set alerts when there is a significant drop in price for your travel destination.

- [Google Flights](https://www.google.com/flights/) - Great for comparing and setting up alerts
- [Skyscanner](https://www.skyscanner.com) - Great for comparing and setting up alerts
- [Kiwi](https://www.kiwi.com/) - Great experience for booking flights on mobile devices
- [Airwander](http://airwander.com/) - Great for booking flights with stopovers

#### Navigation

Offline navigation content is key when you are not certain of an internet connection.

- [Maps.me](http://maps.me/en/home) - Large offline maps, lots of destination content
- [Google Maps](https://www.google.com/maps) - Google maps allows offline downloads for self selectable areas

#### Communication

When you have an internet connection, it can save you quite a bit of money to be able to communicate just using data.

- [Whatsapp](https://www.whatsapp.com/) - Most used app, which supports chat and video/voice calling
- [Telegram](https://telegram.org/) - Great for places where Whatsapp isn't working
- [Slack](https://slack.com/)
- [Google Duo](https://duo.google.com/) - Great for video and/or voice calling, especially on low bandwidth connections

#### Special Shoutouts

Application which are all around wonderful to have when traveling.

- [Google Now](http://www.google.nl/landing/now/) - an intelligent personal assistant developed by Google. Learn more about [Google Now on Wikipedia](https://en.wikipedia.org/wiki/Google_Now).
- [Google Trips](https://get.google.com/trips/) - Makes planning your day very easy with suggestions for things to see and do

#### Handling Currencies

The following tools make paying someone back in their own currency a lot easier.

- [PayPal](https://www.paypal.com)
- [TransferWise](https://transferwise.com/)

#### Splitting Costs

Traveling together or letting someone else pay something for you? This makes it easier to see how costs add up or should be split up.

- [Splitwise](https://www.splitwise.com/) - Supports a great amount of currencies and is well designed

#### Internet Problems and VPNs

- In China most services we use daily are not working due to [the great firewall of China](https://en.wikipedia.org/wiki/Great_Firewall). Whatsapp seems to work though!

VPNs can help you reach services or sites that are blocked, get contents that are not available abroad or let you browse the internet as if you were home (with your own language preselected).

- [ZenMate](https://zenmate.com/) - Great application with a good 7 days free trial
- [Opera](http://www.opera.com/) - Has a built in free VPN

*Note that a VPN may not always work as intended. For example both Netflix and China have blacklisted certain VPNs, limiting their usefulness.*

#### Media and Entertainment

The following services provide offline functionality for their content if you have a paid membership.

- [Spotify](https://www.spotify.com)
- [Soundcloud](https://soundcloud.com)
- [Netflix](https://netflix.com)

#### Power

Electricity is a requirement on your journey abroad. Be it to power your phone or your laptop.

- [Anker powerbanks](https://www.anker.com/) - Great value for the price, including USB-C options
- [Skross world adapter pro](http://www.skross.com/en/product/87/world-adapter-pro.html) - All around great travel adapter, usable in almost every country

#### Secure your data during travels

During your working travel your restricted data could be exposed.
If you feel that your travel frequency may expose your data please keep in mind the following points to ensure that sensitive data contained in your devices will not be compromised:

- VPN - If you are connecting from an untrusted network you should use a VPN connection to avoid [MITM Attack](https://en.wikipedia.org/wiki/Man-in-the-middle_attack) or similar.
- Devices in public places - If you are using your device in a public place someone could read restricted data from your screen, you should protect your screen with a special film that ensure your privacy, [here you can find some samples](https://www.amazon.com/s?k=privacy+screen+filter)
- Speaking in crowded places - Ensure that when you are talking about restricted data you are in a secure place and no-one can hear you.
- 1Password travel mode - If you are approaching travel in a riskly country or you have to leave your devices in an unsecure place, please use [Travel Mode](https://support.1password.com/travel-mode/) in 1Password to ensure that your vaults will be safe in case of device compromission.
