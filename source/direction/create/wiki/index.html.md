---
layout: markdown_page
title: "Category Vision - Wiki"
---

- TOC
{:toc}

## Wiki

<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas, 
and user journeys into this section. -->

Share documentation and organization information with a built in wiki. Each project includes a Wiki rendered by Gollum, and backed by a Git repository.

We are aiming for real time WYSIWYG collaboration (similar to Google Docs) but stored in Markdown and accessible by Git. This should solve the problem of collaborative note taking, be highly approachable for non-technical users, but have the tremendous benefits of storing the content in a portable plain text format that can be cloned, viewed and edited locally (properties of Git).

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=wiki)
- [Epic List](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=wiki)
- [Overall Vision](/direction/create/)

Please reach out to PM James Ramsay ([E-Mail](jramsay@gitlab.com)
[Twitter](https://twitter.com/jamesramsay)) if you'd like to provide feedback or ask
questions about what's coming.

## Target Audience and Experience
<!-- An overview of the personas involved in this category. An overview 
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

## What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

**Next: [Simplify wiki editing](https://gitlab.com/groups/gitlab-org/-/epics/204)** - Currently wiki editing is inconsistent with every other page, both visually and from a workflow perspective. This makes wiki less approachable for users familiar with the rest of GitLab. We should fix these inconsistencies as a priority.

**After: [Improve wiki navigation](https://gitlab.com/groups/gitlab-org/-/epics/700)** - As wikis grow with more and more content, GitLab is not providing the tools necessary to make them easy to navigate and use. This is a problem that can be resolved with support for macros and a few other small improvements.

## Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

We currently most closely compete with **GitHub Wikis** but we should be competing with:

- [Confluence](https://www.atlassian.com/software/confluence)
- [Notion](https://www.notion.so/)
- Google Docs

## Market Research
<!-- This section should link or highlight any relevant market research you've done that justifies our
entry into the market for the particular category. -->

## Business Opportunity
<!-- This section should highlight the business opportunity highlighted by the particular category. -->

## Analyst Landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

- [Wiki navigation issues](https://gitlab.com/groups/gitlab-org/-/epics/700)

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

- [Wiki navigation issues](https://gitlab.com/groups/gitlab-org/-/epics/700)

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

Wiki's are not used internally.

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->

- [Live editing](https://gitlab.com/groups/gitlab-org/-/epics/820)
