---
layout: job_family_page
title: "PR Manager"
---

## PR Manager

The GitLab PR Manager will help GitLab build thought leadership platforms and drive conversation in external communications channels. This position is responsible for developing and overseeing the public relations strategy at GitLab, and will work across teams and the globe to develop and execute public relations campaigns in line with GitLab initiatives. 

### Responsibilities

- Think globally to implement global public relations campaigns.
- Execute thought leadership, product, partner, technical, crisis, rapid response and proactive PR campaigns. 
- Manage GitLab’s PR agency relationship and develop a PR program in line with overall corporate marketing objectives and goals. 
- Work closely with executives, spokespeople and the greater organization to develop press releases, blog posts and media relations strategy for GitLab announcements and news.
- Collaborate across the organization to support the news cycle through various channels, as well as educate teams on news. 
- Oversee the GitLab awards submission program. 
- Respond to daily media inquiries in a timely and professional manner.
- Have your finger on the pulse of the news and provide an overview of interesting news and trends. 
- Report back on press activities, coverage, opportunities, successes and press feedback.
- Measure our PR successes in relation to awareness and impact.
 

### Requirements

- 7+ years experience running public relations efforts at an enterprise technology company.
- Strong media relations skills and a passion for PR.
- A natural storyteller with excellent writing skills. Creative, thoughtful and passionate about storytelling.
- Able to coordinate across many teams and perform in fast-moving startup environment.
- Proven ability to be self-directed and work with minimal supervision.
- Outstanding written and verbal communications skills.
- You share our values, and work in accordance with those values.
- Highly organized, detail-oriented and able to meet deadlines consistently.
